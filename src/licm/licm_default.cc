// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <memory>
#include <thread>
#include <chrono>
#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/finiteelementmap/blockstructuredqkfem.hh>
#include <dune/pdelab/finiteelementmap/qkdg.hh>
#include <dune/pdelab/gridoperator/blockstructured.hh>
#include <dune/pdelab/backend/istl.hh>
#include "dune/ch-generation/grid.hh"
#include "dune/ch-generation/utility.hh"
#include "dune/ch-generation/localoperator/geometry-transformation/default.hh"


int main(int argc, char** argv)
{
  using Dune::PDELab::Backend::native;
  // Maybe initialize MPI
  Dune::FakeMPIHelper::instance(argc, argv);

  unsigned int N = DEFAULT_GRID_SIZE;
  if(argc > 1)
    N = std::stoi(argv[1]);
  auto grid_ptr = Dune::StructuredGridFactory<Grid>::createCubeGrid(Dune::FieldVector<double, dim>(0.),
                                                                    Dune::FieldVector<double, dim>(1.),
                                                                    filled_array<dim>(N));

  GV gv = grid_ptr->leafGridView();

  print_grid_type();

  using FEM = Dune::PDELab::BlockstructuredQkLocalFiniteElementMap<GV, double, double, 1, N_BLOCKS>;
  FEM fem(gv);

  using GFS = Dune::PDELab::GridFunctionSpace<GV, FEM>;
  GFS gfs(gv, fem);
  gfs.update();

  std::cout << "DOF " << gfs.size() << std::endl;

  using MB = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  MB mb(9);

  LOP<dim> lop(gv);

  using GO = Dune::PDELab::GridOperator<GFS, GFS, LOP<dim>, MB, double, double, double>;
  GO go(gfs, gfs, lop, mb);

  using V = typename GO::Domain;
  V x(gfs, 0);
  V y(gfs, 0.);

  x = 1;

  constexpr int max_it = guess_iterations(8, QUAD_ORDER, 3, dim) * (dim == 3 ? 1 : 2);
  std::cout << "IT " << max_it << std::endl;
  for (int it = 0; it < max_it; ++it) {
    go.jacobian_apply(x, y);
  }
  std::cout << "TIME " << lop.dump_timers() << std::endl;
}
