// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <memory>
#include <thread>
#include <chrono>
#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/finiteelementmap/blockstructuredqkfem.hh>
#include <dune/pdelab/finiteelementmap/qkdg.hh>
#include <dune/pdelab/gridoperator/blockstructured.hh>
#include <dune/pdelab/backend/istl.hh>
#include "dune/ch-generation/grid.hh"
#include "dune/ch-generation/utility.hh"
#include "dune/ch-generation/localoperator/defines.hh"

#define ENABLE_HP_TIMERS
#include "@TARGET@_rOperator_file.hh"


template<typename GFSU, typename GFSV, typename LOP,
    typename MB, typename DF, typename RF, typename JF,
    int k,
    typename CU=Dune::PDELab::EmptyTransformation,
    typename CV=Dune::PDELab::EmptyTransformation,
    std::enable_if_t<(k > 2), int> = 0
>
Dune::PDELab::BlockstructuredGridOperator<GFSU, GFSV, LOP, MB, DF, RF, JF, CU, CV> go_switch();
template<typename GFSU, typename GFSV, typename LOP,
    typename MB, typename DF, typename RF, typename JF,
    int k,
    typename CU=Dune::PDELab::EmptyTransformation,
    typename CV=Dune::PDELab::EmptyTransformation,
    std::enable_if_t<(k <= 2), int> = 0
>
Dune::PDELab::GridOperator<GFSU, GFSV, LOP, MB, DF, RF, JF, CU, CV> go_switch();


int main(int argc, char** argv)
{
  using Dune::PDELab::Backend::native;
  // Maybe initialize MPI
  Dune::FakeMPIHelper::instance(argc, argv);

  unsigned int N = @DEFAULT_GRID_SIZE@;
  auto grid_ptr = Dune::StructuredGridFactory<Grid>::createCubeGrid(Dune::FieldVector<double, dim>(0.),
                                                                    Dune::FieldVector<double, dim>(1.),
                                                                    filled_array<dim>(N));

  GV gv = grid_ptr->leafGridView();

  print_grid_type();

  using FEM = Dune::PDELab::BlockstructuredQkLocalFiniteElementMap<GV, double, double, 1, @N_BLOCKS@>;
  FEM fem(gv);

  using GFS = Dune::PDELab::GridFunctionSpace<GV, FEM>;
  GFS gfs(gv, fem);
  gfs.update();

  std::cout << "DOF " << gfs.size() << std::endl;

  using MB = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  MB mb(9);

  rOperator lop(gfs, gfs, Dune::ParameterTree{});

  using GO = decltype(go_switch<GFS, GFS, decltype(lop), MB, double, double, double, @N_BLOCKS@>());
  GO go(gfs, gfs, lop, mb);

  using V = typename GO::Domain;
  V x(gfs, 1.);
  V y(gfs, 0.);

  constexpr int max_it = guess_iterations(8, QUAD_ORDER, 3, dim) * (dim == 3 ? 1 : 2);
  std::cout << "IT " << max_it << std::endl;
  for (int it = 0; it < max_it; ++it) {
    go.jacobian_apply(x, y);
  }
  std::cout << "TIME ";
  lop.dump_timers(std::cout, "", false);
}
