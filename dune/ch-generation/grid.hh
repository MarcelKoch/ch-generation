#pragma once

#ifndef DIMENSION
#define DIMENSION 2
#endif

constexpr int dim = DIMENSION;

#ifdef STRUCTURED
#include <dune/grid/yaspgrid.hh>
using Grid = Dune::YaspGrid<dim>;
void print_grid_type() { std::cout << "GM affine" << std::endl; }
#else
#include <dune/grid/uggrid.hh>
using Grid = Dune::UGGrid<dim>;
void print_grid_type() { std::cout << "GM multilinear" << std::endl; }
#endif
#include <dune/grid/utility/structuredgridfactory.hh>

using GV = typename Grid::LeafGridView;


