#pragma once

#include <array>


constexpr std::size_t quad_size(const std::size_t quadOrder, const std::size_t dim){
  std::size_t _1d = (quadOrder + 2) / 2;  // fast ceil
  std::size_t result = _1d;
  for (int i = 0; i < dim - 1; ++i)
    result *= _1d;
  return result;
}

template<int N, typename T>
constexpr std::array<T, N> filled_array(const T& t){
  std::array<T, N> a{};
  std::fill(begin(a), end(a), t);
  return a;
}

template<typename T>
struct less{
  constexpr bool operator()(const T& a, const T& b) const{
    for (int i = 0; i < a.N(); ++i) {
      if (a[i] < b[i] - 1e-10) return true;
      if (a[i] > b[i] + 1e-10) return false;
    }
    return false;
  }
};

constexpr std::size_t guess_iterations(int max_quad_order, int current_quad_order, int max_dim, int current_dim){
    return std::max(quad_size(max_quad_order, max_dim) / quad_size(current_quad_order, current_dim), 1ul);
}
