#pragma once

#define MCA_START(NAME) __asm volatile("# LLVM-MCA-BEGIN " #NAME);
#define MCA_STOP(NAME) __asm volatile("# LLVM-MCA-END " #NAME);

#define __SSC_MARK(x) do{ __asm__ __volatile__("movl %0, %%ebx; .byte 100, 103, 144" : :"i"(x) : "%ebx"); } while(0)

#ifndef QUAD_ORDER
#define QUAD_ORDER 2
#endif
