#pragma once


#ifndef MAX_VECTOR_SIZE
#define MAX_VECTOR_SIZE 512
#endif

#include <dune/pdelab/localoperator/flags.hh>
#include <dune/pdelab/localoperator/pattern.hh>
#include <dune/pdelab/common/quadraturerules.hh>
#include <dune/pdelab/finiteelement/localbasiscache.hh>
#include <dune/localfunctions/lagrange/lagrangecube.hh>
#include <dune/geometry/multilineargeometry.hh>
#include <dune/common/reservedvector.hh>
#include <dune/common/diagonalmatrix.hh>
#include <dune/codegen/vectorclass/vectorclass.h>
#include "dune/ch-generation/fill_quadrature_cache.hh"
#include "../defines.hh"
#include "../../utility.hh"

#ifdef SIMD8
using SIMD = Vec8d;
constexpr int stride = 8;
SIMD permute(const SIMD& v){
  return permute8<-1,0,1,2,3,4,5,6>(v);
}
SIMD carry(const SIMD& v){
  return permute8<7,-1,-1,-1,-1,-1,-1,-1>(v);
}
#else
using SIMD = Vec4d;
constexpr int stride = 4;
SIMD permute(const SIMD& v){
  return permute4<-1,0,1,2>(v);
}
SIMD carry(const SIMD& v){
  return permute4<3,-1,-1,-1>(v);
}
#endif

class LOP: public Dune::PDELab::LocalOperatorDefaultFlags,
           public Dune::PDELab::FullVolumePattern{
public:
  static constexpr bool doAlphaVolume = true;
  static constexpr bool doPatternVolume = true;

  static constexpr std::size_t k = N_BLOCKS;
  static constexpr std::size_t n_q = quad_size(QUAD_ORDER, 2);
  static constexpr std::size_t n_b = 4;

  template<typename GV>
  explicit LOP(const GV& gv)  {
    const auto e = *gv.template begin<0>();
    const auto geo = e.geometry();
    jit = geo.jacobianInverseTransposed({});
    detjac = geo.integrationElement({});
    fillQuadraturePointsCache(geo, QUAD_ORDER, qp);
    fillQuadratureWeightsCache(geo, QUAD_ORDER, qw);

    for (std::size_t q = 0; q < n_q; ++q) {
      const auto &js_Q1 = cache.evaluateJacobian(qp[q], basis);
      // compute gradients of basis functions in transformed element
      for (std::size_t i = 0; i < n_b; ++i)
        eval_grad[q][i] = js_Q1[i][0];
    }
  }

  void reset_timers() { duration = 0.; }

  auto dump_timers(){
    return duration;
  }

  template<typename R, typename X, typename EG, typename LFSU, typename LFSV>
  void alpha_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, R &r) const { }

  template<typename EG, typename LFSU, typename X, typename LFSV, typename Y>
  void jacobian_apply_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, Y &y) const {
    auto t_start = std::chrono::steady_clock::now();
    std::array<std::array<JacobianType, n_b>, n_q> cached_grad;
    for (std::size_t q = 0; q < n_q; ++q) {
      // compute gradients of basis functions in transformed element
      for (std::size_t i = 0; i < n_b; ++i) {
        cached_grad[q][i] = 0.;
        jit.usmv(SIMD(k), eval_grad[q][i], cached_grad[q][i]);
      }
    }
    for (std::size_t subel_y = 0; subel_y < k; ++subel_y) {
      for (std::size_t subel_x = 0; subel_x < k; subel_x += stride) {
        SIMD y_vec[n_b] = {0, 0, 0, 0};
        SIMD x_vec[n_b];
        for (std::size_t iy = 0; iy < 2; ++iy)
          for (std::size_t ix = 0; ix < 2; ++ix)
            x_vec[ix + 2 * iy].load(&x(lfsv, (subel_y + iy) * (k + 1) + subel_x + ix));

        const auto factor = detjac / double(k * k);

        for (std::size_t q = 0; q < n_q; ++q) {
          SIMD gradu[2] = {0, 0};
          for (std::size_t idim0 = 0; idim0 <= 1; ++idim0)
            for (std::size_t iy = 0; iy < 2; ++iy)
              for (std::size_t ix = 0; ix < 2; ++ix)
                gradu[idim0] += x_vec[ix + 2 * iy] * cached_grad[q][ix + iy * 2][idim0];

          const auto factor_q = factor * qw[q];
          for (std::size_t iy = 0; iy < 2; ++iy)
            for (std::size_t ix = 0; ix < 2; ++ix) {
              y_vec[2 * iy + ix] += (cached_grad[q][ix + iy * 2][0] * gradu[0] +
                                     cached_grad[q][ix + iy * 2][1] * gradu[1]) * factor_q;
            }
        }
        for (std::size_t iy = 0; iy < 2; ++iy)
          for (std::size_t ix = 0; ix < 2; ++ix){
            auto vec = SIMD().load(&y.container()(lfsv, (subel_y + iy) * (k + 1) + subel_x + ix));
            vec += y_vec[2 * iy + ix];
            vec.store(&y.container()(lfsv, (subel_y + iy) * (k + 1) + subel_x + ix));
          }
      }
    }
    duration += std::chrono::duration<double>(std::chrono::steady_clock::now() - t_start).count();
  }

private:
  Dune::DiagonalMatrix<double, 2> jit;
  double detjac;

  using Basis = Dune::Impl::LagrangeCubeLocalBasis<double, double, 2, 1>;
  Basis basis;

  using JacobianType = Dune::FieldVector<SIMD, 2>;
  std::array<std::array<JacobianType, n_b>, n_q> eval_grad;

  Dune::PDELab::LocalBasisCache<Basis> cache;

  Dune::ReservedVector<Dune::FieldVector<double, 2>, n_q> qp;
  Dune::ReservedVector<double, n_q> qw;

  mutable double duration = 0.;
};
