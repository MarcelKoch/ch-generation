#pragma once

#include "dune/pdelab/localoperator/flags.hh"
#include "dune/pdelab/localoperator/pattern.hh"
#include "dune/pdelab/common/quadraturerules.hh"
#include "dune/pdelab/finiteelement/localbasiscache.hh"
#include "dune/localfunctions/lagrange/lagrangecube.hh"
#include "dune/common/reservedvector.hh"
#include "dune/common/diagonalmatrix.hh"
#include "../../fill_quadrature_cache.hh"
#include "../defines.hh"
#include "../../utility.hh"


class LOP: public Dune::PDELab::LocalOperatorDefaultFlags,
           public Dune::PDELab::FullVolumePattern{
public:
  static constexpr bool doAlphaVolume = true;
  static constexpr bool doPatternVolume = true;

  static constexpr std::size_t k = N_BLOCKS;
  static constexpr std::size_t n_q = quad_size(QUAD_ORDER, 2);
  static constexpr std::size_t n_b = 4;

  template<typename GV>
  explicit LOP(const GV& gv)  {
    const auto e = *gv.template begin<0>();
    const auto geo = e.geometry();
    fillQuadraturePointsCache(geo, QUAD_ORDER, qp);
    fillQuadratureWeightsCache(geo, QUAD_ORDER, qw);

    for (std::size_t q = 0; q < n_q; ++q) {
      const auto &js_Q1 = cache.evaluateJacobian(qp[q], basis);
      // compute gradients of basis functions in transformed element
      for (std::size_t i = 0; i < n_b; ++i)
        eval_grad[q][i] = js_Q1[i][0];
    }
  }

  void reset_timers() { duration = 0.; }

  auto dump_timers(){
    return duration;
  }

  template<typename R, typename X, typename EG, typename LFSU, typename LFSV>
  void alpha_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, R &r) const { }

  template<typename EG, typename LFSU, typename X, typename LFSV, typename Y>
  void jacobian_apply_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, Y &y) const {
    const auto geo = eg.entity().geometry();

    auto t_start = std::chrono::steady_clock::now();
#ifdef STRUCTURED
    const auto jit = geo.jacobianInverseTransposed({});
    const auto detjac = geo.integrationElement({});
    const double factor = detjac / double(k * k);

    std::array<std::array<JacobianType, n_b>, n_q> cached_grad;
    for (std::size_t q = 0; q < n_q; ++q) {
      // compute gradients of basis functions in transformed element
      for (std::size_t i = 0; i < n_b; ++i) {
        cached_grad[q][i] = 0.;
        jit.usmv(k, eval_grad[q][i], cached_grad[q][i]);
      }
    }
#else
    std::array<std::array<double, 2>, 2> jacobian = {};
    Dune::FieldMatrix<double, 2, 2> jit = {};
    std::array<Dune::FieldVector<double, 2>, 4> T = {};
    std::array<Dune::FieldVector<double, 2>, 2> T_c = {};
    std::array<Dune::FieldVector<double, 2>, 2> T_r = {};
    Dune::FieldVector<double, 2> T_dc = {};
    Dune::FieldVector<double, 2> T_dr = {};

    for (int i = 0; i < 4; ++i)
      T[i] = geo.corner(i);

    T_r[0] = T[1] - T[0];
    T_r[1] = T[3] - T[2];

    T_c[0] = T[2] - T[0];
    T_c[1] = T[3] - T[1];

    T_dc = T_c[1] - T_c[0];
    T_dr = T_r[1] - T_r[0];
#endif


    for (std::size_t subel_y = 0; subel_y < k; ++subel_y)
      for (std::size_t subel_x = 0; subel_x < k; ++subel_x) {

        double x_l[4] = {}, y_l[4] = {0, 0, 0, 0};
        for (std::size_t iy = 0; iy < 2; ++iy)
          for (std::size_t ix = 0; ix < 2; ++ix)
            x_l [ix + 2 * iy] = x(lfsv, (subel_y + iy) * (k + 1) + subel_x + ix);

        for (std::size_t q = 0; q < n_q; ++q) {
#ifndef STRUCTURED
          Dune::FieldVector<double, 2> p = {(qp[q][0] + subel_x) / k,
                                                   (qp[q][1] + subel_y) / k};
          for (int d = 0; d < 2; ++d) {
            jacobian[d][0] = T_r[0][d] + p[1] * T_dr[d];
            jacobian[d][1] = T_c[0][d] + p[0] * T_dc[d];
          }
          // compute determinant
          const auto detjac = (jacobian[0][0] * jacobian[1][1] - jacobian[1][0] * jacobian[0][1]);
          const double factor = detjac / double(k * k);

          // compute JIT
          const auto detjac_inv = 1./detjac;
          jit[0][0] =  jacobian[0][0] * detjac_inv;   jit[0][1] = -jacobian[1][0] * detjac_inv;
          jit[1][0] = -jacobian[0][1] * detjac_inv;   jit[1][1] =  jacobian[1][1] * detjac_inv;

          std::array<JacobianType, n_b> grad;
          // compute gradients of basis functions in transformed element
          for (std::size_t i = 0; i < n_b; ++i) {
            jit.usmv(k, eval_grad[q][i], grad[i][0]);
          }
#else
          std::array<JacobianType, n_b>& grad = cached_grad[q];
#endif

          double gradu[2] = {};
          for (std::size_t idim0 = 0; idim0 <= 1; ++idim0) {
            for (std::size_t iy = 0; iy < 2; ++iy)
              for (std::size_t ix = 0; ix < 2; ++ix)
                gradu[idim0] += x_l[ix + 2 * iy] * grad[ix + iy * 2][idim0];
          }

          const double factor_q = factor * qw[q];
          for (std::size_t iy = 0; iy < 2; ++iy)
            for (std::size_t ix = 0; ix < 2; ++ix)
              y_l[ix + 2 * iy] +=  (grad[ix + iy * 2][0] * gradu[0] +
                                    grad[ix + iy * 2][1] * gradu[1]) * factor_q;
        }
        for (std::size_t iy = 0; iy < 2; ++iy)
          for (std::size_t ix = 0; ix < 2; ++ix)
            y.accumulate(lfsv, (subel_y + iy) * (k + 1) + subel_x + ix, y_l[ix + 2 * iy]);
      }
    duration += std::chrono::duration<double>(std::chrono::steady_clock::now() - t_start).count();
  }

private:
  using Basis = Dune::Impl::LagrangeCubeLocalBasis<double, double, 2, 1>;
  Basis basis;

  using JacobianType = Dune::FieldVector<double, 2>;
  std::array<std::array<JacobianType, n_b>, n_q> eval_grad;

  Dune::PDELab::LocalBasisCache<Basis> cache;

  Dune::ReservedVector<Dune::FieldVector<double, 2>, n_q> qp;
  Dune::ReservedVector<double, n_q> qw;

  mutable double duration = 0.;
};
