#ifndef BLOCKSTRUCTURED_PERFORMANCE_ANALYSIS_IMPLEMENTATIONS_Q1_LOP_HH
#define BLOCKSTRUCTURED_PERFORMANCE_ANALYSIS_IMPLEMENTATIONS_Q1_LOP_HH

#include "dune/pdelab/localoperator/flags.hh"
#include "dune/pdelab/localoperator/pattern.hh"
#include "dune/pdelab/common/quadraturerules.hh"
#include "dune/pdelab/finiteelement/localbasiscache.hh"
#include "dune/localfunctions/lagrange/lagrangecube.hh"
#include "dune/common/reservedvector.hh"
#include "dune/common/diagonalmatrix.hh"
#include "../../fill_quadrature_cache.hh"
#include "../defines.hh"

template<int dim>
class LOP: public Dune::PDELab::LocalOperatorDefaultFlags,
    public Dune::PDELab::FullVolumePattern{
  using Basis = Dune::Impl::LagrangeCubeLocalBasis<double, double, dim, 1>;
  Basis basis;

  using JacobianType = typename Basis::Traits::JacobianType;

  Dune::PDELab::LocalBasisCache<Basis> cache;

  static constexpr std::size_t n_b = Dune::power(2, dim);
  mutable double duration = 0.;
public:
  static constexpr bool doAlphaVolume = true;
  static constexpr bool doPatternVolume = true;

  template<typename GV>
  explicit LOP(const GV& gv) {}

  void reset_timers() { duration = 0.; }

  auto dump_timers(){
    return duration;
  }

  template<typename R, typename X, typename EG, typename LFSU, typename LFSV>
  void alpha_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, R &r) const { }

  template<typename EG, typename LFSU, typename X, typename LFSV, typename Y>
  void jacobian_apply_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, Y &y) const {
    auto cell_geo = eg.entity().geometry();
    const auto quadrature_rule = Dune::quadratureRule(cell_geo, QUAD_ORDER);

    auto t_start = std::chrono::steady_clock::now();
#ifdef STRUCTURED
    const auto jit = cell_geo.jacobianInverseTransposed({});
    const auto detjac = cell_geo.integrationElement({});
#endif
    for (const auto &qp: quadrature_rule) {
      const auto &js_Q1 = cache.evaluateJacobian(qp.position(), basis);

#ifndef STRUCTURED
      const auto jit = cell_geo.jacobianInverseTransposed(qp.position());
      const auto detjac = cell_geo.integrationElement(qp.position());
#endif

      std::array<JacobianType, n_b> grad = {};
      for (int i = 0; i < n_b; ++i) {
        jit.umv(js_Q1[i][0], grad[i][0]);
      }
      double gradu[2] = {};
      for (std::size_t idim0 = 0; idim0 <= 1; ++idim0) {
        for (std::size_t i = 0; i < n_b; ++i)
          gradu[idim0] += x(lfsv, i) * grad[i][0][idim0];
      }
      const auto factor = detjac * qp.weight();
      for (std::size_t i = 0; i < n_b; ++i)
        y.accumulate(lfsv, i,(grad[i][0][0] * gradu[0] + grad[i][0][1] * gradu[1]) * factor);
    }
    duration += std::chrono::duration<double>(std::chrono::steady_clock::now() - t_start).count();
  }
};

#endif //BLOCKSTRUCTURED_PERFORMANCE_ANALYSIS_IMPLEMENTATIONS_Q1_LOP_HH
