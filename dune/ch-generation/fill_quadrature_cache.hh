#pragma once

#include <dune/pdelab/common/quadraturerules.hh>

template <typename G, typename T>
void fillQuadratureWeightsCache(const G& geo, const int quadOrder, T& quadratureWeights){
  if(quadratureWeights.size() != 0)
    return;
  else{
    for (const auto& qp : quadratureRule(geo, quadOrder)){
      if(quadratureWeights.size() < 8)
        quadratureWeights.push_back(qp.weight());
    }
  }
}


template <typename G, typename T>
void fillQuadraturePointsCache(const G& geo, const int quadOrder, T& quadraturePoints){
  if(quadraturePoints.size() != 0)
    return;
  else{
    for (const auto& qp : quadratureRule(geo, quadOrder)){
      if(quadraturePoints.size() < 8)
        quadraturePoints.push_back(qp.position());
    }
  }
}
